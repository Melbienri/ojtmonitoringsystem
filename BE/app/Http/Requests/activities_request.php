<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class activities extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activities' => ['required', 'bail', 'max:255'],
            'output' =>     ['required', 'bail', 'max:255'],
            'student_id' => ['exists:student,id', 'bail', 'required', 'integer'],
            'adviser_id' => ['exists:adviser,id', 'required', 'integer']
        ];
    }
}
