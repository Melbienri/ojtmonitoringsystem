<?php

namespace App\Http\Controllers;

use App\Models\student;
use App\Http\Controllers\Controller;
use App\Http\Requests\student_request;

class register_controller extends Controller
{
    public function index(){
        
    }
    public function show(){

    }
    public function store(student_request $request){
        return response()->json(student::create($request->all()));
    }
    public function update(){
        
    }
    public function destroy(){
        
    }
}
