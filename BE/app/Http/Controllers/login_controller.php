<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\login_request;
use App\Http\Requests\teacher_login_request;

class login_controller extends Controller
{
    public function student_login(login_request $request){

        if (Auth::attempt($request->only('student_number', 'password'))){
            return response()->json(Auth::user(), 200);
        }
        throw ValidationException::withMessages([
            'message' =>['The provided credentials are incorect.']
        ]);
    }
    public function teacher_login(teacher_login_request $request){
        if (Auth::attempt($request->only('student_number', 'password'))){
            return response()->json(Auth::user(), 200);
        }
        throw ValidationException::withMessages([
            'message' =>['The provided credentials are incorect.']
        ]);
    }
        public function logout(){
            Auth::logout();
        }
}
