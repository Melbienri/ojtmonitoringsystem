import Vue from 'vue'
 import VueRouter from 'vue-router'
 import Landing from '../components/pages/Main/Landing'
 
 Vue.use(VueRouter)
 
 const routes = [
   {
     path: '/',
     
     name: 'Main',
     component: Landing
   },
   {
    path: '/Login',
    name: 'Login Form Student',
    component: () => import('@/components/pages/Login/Login')
  },
  {
    path: '/Register',
    name: 'Registration Form Student',
    component: () => import('@/components/pages/Login/Register')
  },
  {
    path: '/RegisterTeacher',
    name: 'Registration Form Teacher',
    component: () => import('@/components/pages/Login/RegisterTeacher')
  },
  {
    path: '/LoginTeacher',
    name: 'Login Form Teacher',
    component: () => import('@/components/pages/Login/LoginTeacher')
  },
  {
    path: '/StudentInfo',
    name: 'Student Information',
    component: () => import('@/components/pages/StudentInfo/StudentInfo')
  },
  {
    path: '/Attendance',
    name: 'Attendance',
    component: () => import('@/components/pages/StudentInfo/Attendance')
  },
  {
    path: '/DailyReport',
    name: 'Daily Report',
    component: () => import('@/components/pages/StudentInfo/DailyReport')
  },
  {
    path: '/AdviserInfo',
    name: 'Adviser Information',
    component: () => import('@/components/pages/TeacherInfo/AdviserInfo')
  },
  {
    path: '/StudentList',
    name: 'Student Lists',
    component: () => import('@/components/pages/TeacherInfo/StudentList')
  },
  {
    path: '/StudentAttendance',
    name: 'Student Attendance',
    component: () => import('@/components/pages/TeacherInfo/StudentAttendance')
  },
  {
    path: '/StudentReport',
    name: 'Student Report',
    component: () => import('@/components/pages/TeacherInfo/StudentReport')
  },
   
 ]
 
 const router = new VueRouter({
   mode: 'history',
   base: process.env.BASE_URL,
   routes
})

export default router